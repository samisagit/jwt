package jwt

import (
	"crypto"
	"crypto/rand"
	cryptor "crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

type testRSAHasher struct {
	pk *rsa.PrivateKey
}

func (hv testRSAHasher) HashFunc(header, payload interface{}) (string, error) {
	jsonHeader, err := json.Marshal(header)
	if err != nil {
		return "", err
	}
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}

	message := []byte(fmt.Sprintf("%s.%s", b64.RawURLEncoding.EncodeToString(jsonHeader), b64.RawURLEncoding.EncodeToString(jsonPayload)))
	hashed := sha512.Sum512(message)
	signature, err := rsa.SignPKCS1v15(cryptor.Reader, hv.pk, crypto.SHA512, hashed[:])
	if err != nil {
		return "", err
	}

	return string(signature), nil
}

func TestRSAJourney(t *testing.T) {
	pkey, err := generatePrivateKey(2048)
	if err != nil {
		t.Error(err)
	}

	hv := testRSAHasher{
		pk: pkey,
	}

	s := NewService(&ServiceConfig{
		Environment:  "test",
		ExpiryOffset: time.Second * 3,
		Algorithm:    "RS512",
	}, hv)

	tokenString, _, err := s.NewToken("testID", time.Now().UnixNano())
	if err != nil {
		t.Fatal(err)
	}

	token, err := s.ExtractToken(tokenString)
	if err != nil {
		t.Fatal(err)
	}

	if err = s.ValidateToken(token); err != nil {
		t.Errorf("expected token to be valid, got error: %s", err.Error())
	}

	time.Sleep(time.Second * 3)

	if err = s.ValidateToken(token); err == nil {
		t.Errorf("expected token to be invalid")
	}
}

func TestWireIdempotency(t *testing.T) {
	pkey, err := generatePrivateKey(2048)
	if err != nil {
		t.Error(err)
	}

	hv := testRSAHasher{
		pk: pkey,
	}

	s := NewService(&ServiceConfig{
		Environment:  "test",
		ExpiryOffset: time.Second * 3,
		Algorithm:    "RS512",
	}, hv)

	tokenString, _, err := s.NewToken("testID", time.Now().UnixNano())
	if err != nil {
		t.Fatal(err)
	}

	token, err := s.ExtractToken(tokenString)
	if err != nil {
		t.Fatal(err)
	}

	postWireTokenString, err := token.Wire()
	if err != nil {
		t.Fatal(err)
	}

	if postWireTokenString != tokenString {
		t.Errorf("expected post to be\n%s\ngot\n%s", tokenString, postWireTokenString)
	}
}

func generatePrivateKey(bitSize int) (*rsa.PrivateKey, error) {
	// Private Key generation
	privateKey, err := rsa.GenerateKey(rand.Reader, bitSize)
	if err != nil {
		return nil, err
	}

	// Validate Private Key
	err = privateKey.Validate()
	if err != nil {
		return nil, err
	}

	return privateKey, nil
}
