package jwt

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
)

// token is a representation of a generic JWT
type Token struct {
	// Header contains JWT header fields
	Header TokenHeader `json:"header"`
	// Payload contains authorization fields
	Payload TokenPayload `json:"payload"`
	// Signature contains a signed hash of the payload using the algorithm from the header
	Signature TokenSignature `json:"signature"`
}

type TokenHeader struct {
	Alg string `json:"alg"`
	Typ string `json:"typ"`
}

type TokenPayload struct {
	ID          string `json:"ID"`
	Environment string `json:"environment"`
	Timestamp   int64  `json:"timestamp"`
}

type TokenSignature struct {
	Hash string `json:"hash"`
}

func (t *Token) Wire() (string, error) {
	if t == nil {
		return "", fmt.Errorf("token is nil")
	}

	header, err := json.Marshal(t.Header)
	if err != nil {
		return "", err
	}

	payload, err := json.Marshal(t.Payload)
	if err != nil {
		return "", err
	}

	b64Header := b64.RawURLEncoding.EncodeToString(header)
	b64Payload := b64.RawURLEncoding.EncodeToString(payload)
	b64Signature := b64.RawURLEncoding.EncodeToString([]byte(t.Signature.Hash))

	return fmt.Sprintf("%s.%s.%s", b64Header, b64Payload, b64Signature), nil
}
