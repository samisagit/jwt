package jwt

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

// ServiceConfig is used to configure the JWT service
type ServiceConfig struct {
	// Environment is used to check the payload is for this environment
	Environment string `json:"environment"`
	// ExpiryOffset is used to validate the token is in date
	ExpiryOffset time.Duration `json:"expiryOffset"`
	// Algorithm is used in the header of JWTs
	Algorithm string `json:"algorithm"`
}

// ConfigFromFile creates a ServiceConfig type with data from the file at the specified path
func ConfigFromFile(configFilePath string) (*ServiceConfig, error) {
	file, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, err
	}

	conf := ServiceConfig{}
	err = json.Unmarshal(file, &conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

type hasher interface {
	HashFunc(header, payload interface{}) (string, error)
}

type Service struct {
	// environment is used to check the payload is for this environment
	environment string
	// expiryOffset is used to validate the token is in date
	expiryOffset time.Duration
	// algorithm is used in the header of JWTs
	algorithm string
	// hv is used to sign and validate JWTs
	hv hasher
}

// NewService returns a JWT validation service using the provided secret, environment and expiry offset
func NewService(
	config *ServiceConfig,
	hv hasher,
) Service {
	return Service{
		environment:  config.Environment,
		expiryOffset: config.ExpiryOffset,
		algorithm:    config.Algorithm,
		hv:           hv,
	}
}

// ValidateToken checks the provided token is valid, in date and for this environment
func (s Service) ValidateToken(token *Token) error {
	if token.Payload.Environment != s.environment {
		return fmt.Errorf("incorrect environment")
	}

	now := time.Now().UnixNano()
	if now-token.Payload.Timestamp >= s.expiryOffset.Nanoseconds() {
		return fmt.Errorf("token expired: expired by %f", time.Duration((now-token.Payload.Timestamp)-s.expiryOffset.Nanoseconds()).Seconds())
	}

	isValid := s.verify(token)
	if !isValid {
		return fmt.Errorf("invalid signature")
	}

	return nil
}

func (s Service) verify(token *Token) bool {
	hash, err := s.hv.HashFunc(token.Header, token.Payload)
	if err != nil {
		return false
	}

	return hash == token.Signature.Hash
}

func (s Service) assignSignature(token *Token) error {
	hash, err := s.hv.HashFunc(token.Header, token.Payload)
	if err != nil {
		return err
	}
	token.Signature.Hash = hash

	return nil
}

// extractToken creates a token from the provided string and errors if it is invalid
func (s Service) ExtractToken(jwt string) (*Token, error) {
	if jwt == "" {
		return nil, fmt.Errorf("JWT value not valid")
	}

	tokenParts := strings.Split(jwt, ".")
	if len(tokenParts) != 3 {
		return nil, fmt.Errorf("token should be period separated")
	}

	extractedToken := Token{}

	decodedHeader, _ := b64.RawURLEncoding.DecodeString(tokenParts[0])
	err := json.Unmarshal(decodedHeader, &extractedToken.Header)
	if err != nil {
		return nil, fmt.Errorf("token key Header invalid")
	}

	decodedPayload, _ := b64.RawURLEncoding.DecodeString(tokenParts[1])
	err = json.Unmarshal(decodedPayload, &extractedToken.Payload)
	if err != nil {
		return nil, fmt.Errorf("token key Payload invalid")
	}

	decodedSignature, _ := b64.RawURLEncoding.DecodeString(tokenParts[2])
	extractedToken.Signature = TokenSignature{
		Hash: string(decodedSignature),
	}

	return &extractedToken, nil
}

func (s Service) NewToken(ID string, timestamp int64) (string, []byte, error) {
	token := Token{}
	token.Header = TokenHeader{
		Alg: s.algorithm,
		Typ: "JWT",
	}
	token.Payload = TokenPayload{
		ID:          ID,
		Environment: s.environment,
		Timestamp:   timestamp,
	}
	err := s.assignSignature(&token)
	if err != nil {
		return "", nil, err
	}

	headerBytes, err := json.Marshal(token.Header)
	if err != nil {
		return "", nil, err
	}
	payloadBytes, err := json.Marshal(token.Payload)
	if err != nil {
		return "", nil, err
	}

	return fmt.Sprintf(
		"%s.%s.%s",
		b64.RawURLEncoding.EncodeToString(headerBytes),
		b64.RawURLEncoding.EncodeToString(payloadBytes),
		b64.RawURLEncoding.EncodeToString([]byte(token.Signature.Hash)),
	), payloadBytes, nil
}
